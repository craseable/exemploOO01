#ifndef ALUNO_HPP
#define ALUNO_HPP

#include<string>
#include "pessoa.hpp"

using namespace std;

class Aluno : public Pessoa {
private:
	//Atributos
	string curso;
	float IRA;
public:
	//Metodos
	Aluno();//construtor
	~Aluno(); //destrutor
	float getIRA();
//	void calculaIRA();
	string getCurso();
	void setCurso(string curso);


};

#endif
