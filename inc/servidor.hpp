#ifndef SERVIDOR_HPP
#define SERVIDOR_HPP

#include<string>
#include "pessoa.hpp"

using namespace std;

class Servidor : public Pessoa {
private:
	//Atributos
	string cargo;
	string departamento;
	int carga_horaria;;
public:
	//metodos
	string getCargo();
	void setCargo(string cargo);
	string getDepartamento();
	void setDepartamento(string departamento);
	int getCarga_Horaria();
	void setCarga_Horaria(int carga_horaria);
};

#endif
