#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include<string>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa {
private: 
	//Atributos
	string formacao;
	string departamento;
	float indice_de_aprovacao;
public:
	//metodos
	Professor(); //construtor
	~Professor();//destrutor
	string getFormacao();
	void setFormacao(string formacao);
	string getDepartamento();
	void setDepartamenteo(string departamento);
	float getIndice();
	void setIndice(float indice_de_aprovacao);

};

#endif
