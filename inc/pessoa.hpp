#ifndef PESSOA_HPP
#define PESSOA_HPP

#include<string>

using namespace std;

class Pessoa {
	//Atributos
private:
	int matricula;
	string nome, telefone, endereco;
	long int cpf;
	
	//Metodos
public: 
	Pessoa(); //construtor vazio
	~Pessoa(); //destrutor vazio
	//Metodos acessadores
	int getMatricula();
	void setMatricula(int matricula);
	string getNome();
	void setNome(string nome);
	string getTelefone();
	void setTelefone(string telefone);
	string getEndereco();
	void setEndereco(string endereco);	
	long int getCpf();
	void setCpf(long int cpf);
	
	
};
#endif
